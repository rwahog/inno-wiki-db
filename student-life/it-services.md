<!-- TITLE: It Services -->
<!-- SUBTITLE: A quick summary of It Services -->

# Mail client settings 
Enter the following information:
Email address: enter your Email in the domain @edu.innopolis.ru example@edu.innopolis.ru.
Password: enter password of your Email.

Press "Next".
Choose account "Exchange".
Set the following settings of server "Exchange":
Domain\Username: enter example@innopolis.ru
Password: enter password of your Email.

Server : enter the server name "ActiveSync mail.university.innopolis.ru". 
Choose option "Use secure connection (SSL)".
Skip option "Accept all SSL certificates".

Press "Next". 
Choose the following options (or leave the default settings):
Email checking frequency (Automatic/ Never /Every 5 minutes /Every 10 minutes /Every 15 minutes /Every 30 minutes),
Amount to synchronize (One day /Three days /One week /Two weeks /One month),
Sent email from this account by default (Yes /No),
Notify me when email arrives (Yes/No),
Sync contacts from this account (Yes/No).

Press "Next".
Enter nickname (not necessary) and your name.
Press "Done" to finish.